# README #

This repository is an application made for the Shaw and Partners recruiting program.

### Technologies used: ###

* Java EE using openjdk-14
* Apache Tomcat 8.5.57
* Intellij Idea Ultimate
* Apache Maven
* JUnit 5

### How do I get set up? ###

* It's preferable to use the Intellij Idea IDE, this way it only requires to create a new project,
enable Version Control, clone this repository and set up Tomcat
* It's also possible to run it at any other IDE, Maven should handle any dependencies
* A Live APP of this code is being hosted at Heroku, link for the app: https://enigmatic-oasis-59556.herokuapp.com/
* JUnit test package is provided at the src/test folder

### Notes: ###
* It must be noted that the GitHub API has a limit of 5000 requests within a 60-minute window
even if the user is authenticated, the GitHub will the  error code return 403 FORBIDDEN,
which produces errors that were properly treated

### Who do I talk to? ###

* Repository owner, send an e-mail to osman@uel.br