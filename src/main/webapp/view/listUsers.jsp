<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>

    <title>Github API</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        #tabela {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tabela td, #tabela th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        #tabela tr:nth-child(even){background-color: #f2f2f2;}

        #tabela tr:hover {background-color: #ddd;}

        #tabela th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #000000;
            color: white;
        }
    </style>

    <style>
        .buttons {
            width: 200px;
            margin: 0 auto;
            display: inline;}

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
    </style>

</head>
<body>

<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Github API</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

    <br><br><br>
    <h2 class="text-center">List of found users</h2>

    <br>
    <table id="tabela">
        <tr>
            <th>ID</th>
            <th>Login</th>
            <th>Link</th>
        </tr>
        <c:forEach var="user" items="${requestScope.userList}">
                <tr>
                    <td><c:out value="${user.id}" /></td>
                    <td><c:out value="${user.login}" /></td>
                    <td><a href="${pageContext.request.contextPath}/userProfile?login=${user.login}">${user.profileURL}</a></td>
                </tr>
        </c:forEach>
    </table>

    <br>
    <form class="form" action="${pageContext.request.contextPath}/listUsers" method="GET">
        <div class="form-group">
            <div class="text-center">
                <button class="action_btn next" name="id" value="${requestScope.nextUser}" type="submit">Next</button>
            </div>
        </div>
    </form>

</body>
</html>
