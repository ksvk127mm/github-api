package com.uel.API;

import com.uel.Exceptions.RequestLimitException;
import com.uel.model.Repository;
import com.uel.model.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Github
{
    private static String performRequest(String url_str) throws RequestLimitException
    {
        try {
            URL url = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader reader;
            // Request setup
            connection.setRequestMethod("GET");
            // 5 seconds timeout
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            // Sending the request
            int status = connection.getResponseCode();
            // Response utilities
            String line;
            StringBuilder responseContent = new StringBuilder();

            if (status > 299 ) {
                // status > 299 indicates a failed request
                connection.disconnect();
                if(status == 403) {
                    throw new RequestLimitException("ERROR 403: Requests per hour limit reached");
                }
            } else {
                // status <= 299 indicates a sucessful request
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    responseContent.append(line);
                }
                connection.disconnect();
                reader.close();
            }
            return responseContent.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    // GET - /api/users/:username/details
    public static User getUserDetails(String url) throws RequestLimitException
    {
        String responseBody = performRequest(url);

        // Constructing a JSONObject from the response String
        if(responseBody != null) {
            try{
                JSONObject jsonObject = new JSONObject(responseBody);
                User user = new User();
                user.setId(jsonObject.getInt("id"));
                user.setLogin(jsonObject.getString("login"));
                user.setProfileURL(jsonObject.getString("html_url"));
                user.setCreatDate(jsonObject.getString("created_at"));
                user.setFoto_url(jsonObject.getString("avatar_url"));
                return user;
            } catch (JSONException ex){
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        } else
            return null;
    }

    // GET - /api/users?since={number}
    public static List<User> getUsers(String url) throws RequestLimitException
    {
        String responseBody = performRequest(url);
        List<User> userList = new ArrayList<>();

        if(responseBody != null){
            try {
                // Constructing a JSONArray from the response String
                JSONArray array = new JSONArray(responseBody);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    User user = new User();
                    user.setId(object.getInt("id"));
                    user.setLogin(object.getString("login"));
                    user.setProfileURL(object.getString("html_url"));
                    userList.add(user);
                }
                return userList;
            } catch (JSONException ex){
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        } else
            return null;
    }

    // GET - /api/users/:username/repos
    public static List<Repository> getRepositories(String url) throws RequestLimitException
    {
        String responseBody = performRequest(url);
        List<Repository> repoList = new ArrayList<>();

        if(responseBody != null){
            try {
                // Constructing a JSONArray from the response String
                JSONArray array = new JSONArray(responseBody);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    Repository repo = new Repository();
                    repo.setId(object.getInt("id"));
                    repo.setName(object.getString("name"));
                    repo.setUrl(object.getString("html_url"));
                    repoList.add(repo);
                }
                return repoList;
            } catch (JSONException ex){
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        } else
            return null;
    }
}
