package com.uel.Exceptions;

public class RequestLimitException extends Exception
{
    public RequestLimitException(String errorMessage){
        super(errorMessage);
    }
}
