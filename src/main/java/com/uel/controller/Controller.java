package com.uel.controller;

import java.io.*;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import com.uel.API.Github;
import com.uel.Exceptions.RequestLimitException;
import com.uel.model.Repository;
import com.uel.model.User;
import org.json.JSONException;


@WebServlet(name = "Controller",
urlPatterns = {"/listUsers",
               "/choice",
               "/userProfile"})

public class Controller extends HttpServlet
{
    // GitHub domain used at all endpoints
    private final String domain = "https://api.github.com/users";
    // GitHub API key, a must for execution
    private final String API_KEY = "client_id=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";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        RequestDispatcher dispatcher;
        String url_str;

        switch (request.getServletPath()){
            // index.jsp submit, forwards to /listUsers or /userProfile according to the user's selection
            case "/choice":
                if (request.getParameter("choice").equals("id"))
                    dispatcher = request.getRequestDispatcher("listUsers");
                else
                    dispatcher = request.getRequestDispatcher("userProfile?login=" + request.getParameter("id"));
                dispatcher.forward(request, response);
                break;
            // "Search by Id" option, lists users with id as an anchor and forwards to view/listUsers.jsp
            case "/listUsers":
                url_str = domain + "?since=" + request.getParameter("id") + "&" + API_KEY;
                try {
                    List<User> userList = Github.getUsers(url_str);
                    if (userList != null) {
                        request.setAttribute("userList", userList);
                        request.setAttribute("nextUser", userList.get(userList.size() - 1).getId());
                        dispatcher = request.getRequestDispatcher("view/listUsers.jsp");
                    } else {
                        request.setAttribute("error", "usersNotFound");
                        dispatcher = request.getRequestDispatcher("");
                    }
                } catch (RequestLimitException ex){
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                    request.setAttribute("error", "request_limit");
                    dispatcher = request.getRequestDispatcher("");
                }
                dispatcher.forward(request, response);
                break;
            // "Find by Username" option, searches for the user and his repositories and forwards to view/profile.jsp
            case "/userProfile":
                url_str = domain + "/" + request.getParameter("login");
                try {
                    User user = Github.getUserDetails(url_str + "?" + API_KEY);
                    if (user != null) {
                        String page = request.getParameter("page");
                        url_str = url_str + "/repos?" + API_KEY + (page != null ? "&page=" + page : "");
                        List<Repository> repos = Github.getRepositories(url_str);
                        if (repos != null) {
                            if (repos.size() > 0) {
                                if (page != null)
                                    request.setAttribute("page", Integer.parseInt(page));
                                else
                                    request.setAttribute("page", 1);
                                request.setAttribute("repos", repos);
                            } else {
                                if (page != null) {
                                    url_str = domain + "/" + request.getParameter("login") + "/repos?page="
                                            + (Integer.parseInt(page) - 1) + "&" + API_KEY;
                                    repos = Github.getRepositories(url_str);
                                    request.setAttribute("repos", repos);
                                    request.setAttribute("page", Integer.parseInt(page) - 1);
                                }
                            }
                        }
                        request.setAttribute("user", user);
                        dispatcher = request.getRequestDispatcher("view/profile.jsp");
                    } else {
                        request.setAttribute("error", "userNotFound");
                        dispatcher = request.getRequestDispatcher("");
                    }
                } catch (RequestLimitException ex){
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                    request.setAttribute("error", "request_limit");
                    dispatcher = request.getRequestDispatcher("");
                }
                dispatcher.forward(request, response);
                break;
        }
    }
}