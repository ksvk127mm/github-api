package com.uel.demo;

import com.uel.API.Github;
import com.uel.Exceptions.RequestLimitException;
import com.uel.model.Repository;
import com.uel.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class Tests
{
    // GitHub API key, a must for execution
    private final String API_KEY = "client_id=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";
    // GitHub API domain
    private final String domain = "https://api.github.com/users";
    // Set this variable for getUsersSinceTest()
    private int userId = 1;
    // Set this variable for getUserTest() or getRepositoriesTest()
    private String username = "KhalifaOsman";
    // Keep this one "true"
    private boolean test_details = true;

    // GET - /api/users?since={number} test
    @Test
    void getUsersSinceTest()
    {
        String url_str = domain + "?since=" + userId + "&" + API_KEY;
        try {
            List<User> userList = Github.getUsers(url_str);
            Assertions.assertNotNull(userList,"Assert userList not null test failed");
            // Running getUserTest() and getRepositoriesTest() for each user at userList
            test_details = false;
            for(User user : userList){
                username = user.getLogin();
                getUserTest();
                getRepositoriesTest();
            }

            System.out.println("getUsersSinceTest(" + userId + "): test success!");
        } catch (RequestLimitException ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    // GET - /api/users/:username/details test, used by getUsersSinceTest() and can be executed separately
    @Test
    void getUserTest()
    {
        String url_str = domain + "/" + username + "?" + API_KEY;
        try {
            User user = Github.getUserDetails(url_str);

            Assertions.assertNotNull(user, "Assert user not null test failed");
            Assertions.assertTrue(user.getId() > 0, "Assert user.id > 0 test failed");
            Assertions.assertNotNull(user.getLogin(), "Assert user.login not null test failed");
            Assertions.assertNotNull(user.getProfileURL(), "Assert user.profileURL not null test failed");
            if (test_details) {
                Assertions.assertNotNull(user.getCreatDate(), "Assert user.creatDate not null test failed");
                Assertions.assertNotNull(user.getFoto_url(), "Assert user.foto_url not null test failed");
            }

            System.out.println("getUserTest(" + username + "): test success!");
        } catch (RequestLimitException ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    // GET - /api/users/:username/repos test, used by getUsersSinceTest() and can be executed separately
    @Test
    void getRepositoriesTest()
    {
        String url_str = domain + "/" + username + "/repos?" + API_KEY;
        try {
            List<Repository> repos = Github.getRepositories(url_str);

            Assertions.assertNotNull(repos, "Assert repos not null test failed");
            for (Repository repo : repos) {
                Assertions.assertTrue(repo.getId() > 0, "Assert repo.id > 0 test failed");
                Assertions.assertNotNull(repo.getName(), "Assert repo.name not null test failed");
                Assertions.assertNotNull(repo.getUrl(), "Assert repo.url not null test failed");
            }

            System.out.println("getRepositoresTest(" + username + "): test success!");
        } catch (RequestLimitException ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
