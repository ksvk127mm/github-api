<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>

<html>
<head>

    <title>Github API</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</head>
<body>

<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Github API</a>
            </div>
        </div>
    </nav>
</header>

<main role="main">
    <br><br><br><br>
    <div class="container">
        <h2 class="text-center">Select a user id for listing</h2>
        <form class="form" action="${pageContext.request.contextPath}/choice" method="GET">
            <div class="form-group">
                <label class="control-label" for="id">ID or Username</label>
                <input id="id" class="form-control" type="text" name="id"/>
                <br>
                <label class="control-label" for="choice">Options</label>
                <select class="form-control" name="choice" id="choice">
                    <option value="id">Search by Id</option>
                    <option value="name">Find by Username</option>
                </select>
                <c:if test = "${requestScope.error.equals(\"userNotFound\") || requestScope.error.equals(\"usersNotFound\")}">
                    <h3 class="text-center" style="color: red">Error: user not found</h3>
                </c:if>
                <c:if test="${requestScope.error.equals(\"request_limit\")}">
                    <h3 class="text-center" style="color: red">Error 403: requests per hour limit reached</h3>
                </c:if>
                <br>
                <div class="text-center">
                    <button class="btn btn-lg btn-primary" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
</main>

</body>
</html>