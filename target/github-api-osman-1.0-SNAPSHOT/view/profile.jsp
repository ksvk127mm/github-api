<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>

    <title>Github API</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        #tabela {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tabela td, #tabela th {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        #tabela tr:nth-child(even){background-color: #f2f2f2;}

        #tabela tr:hover {background-color: #ddd;}

        #tabela th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            background-color: #000000;
            color: white;
        }
    </style>

    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 300px;
            margin: auto;
            text-align: center;
        }

        .date {
            color: grey;
            font-size: 14px;
        }
    </style>

    <style>
        .buttons {
            width: 200px;
            margin: 0 auto;
            display: inline;}

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
    </style>

</head>
<body>

<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Github API</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li><a href="${pageContext.request.contextPath}/listUsers?id=${requestScope.user.id - 1}">Return to Users</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<br><br><br>

<div class="card">
    <img src="${requestScope.user.foto_url}" alt="Image not found" style="width:100%">
    <h1>${requestScope.user.login}</h1>
    <p class="date">Since ${requestScope.user.creatDate}</p>
    <p>ID: ${requestScope.user.id}</p>
    <a style="text-decoration: none;font-size: 22px;color: black;" href="${requestScope.user.profileURL}"><i class="fa fa-github"></i></a>
</div>


<br><br><br>
<c:if test="${requestScope.repos != null && requestScope.repos.size() > 0}">
    <h2 class="text-center">${requestScope.user.login}'s Repositories</h2>
    <br>
    <table id="tabela">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Link</th>
        </tr>
        <c:forEach var="repo" items="${requestScope.repos}">
            <tr>
                <td><c:out value="${repo.id}" /></td>
                <td><c:out value="${repo.name}" /></td>
                <td><a href="${repo.url}">${repo.url}</a></td>
            </tr>
        </c:forEach>
    </table>

    <br>
    <div class="text-center">
        <form class="form" action="${pageContext.request.contextPath}/userProfile" method="GET">
            <div class="buttons">
                <input type="hidden" name="login" value="${requestScope.user.login}">
                <c:if test = "${requestScope.page > 1}">
                    <button class="action_btn previous" name="page" value="${requestScope.page - 1}" type="submit">Previous</button>
                </c:if>
                <button class="action_btn next" name="page" value="${requestScope.page + 1}" type="submit">Next</button>
            </div>
        </form>
    </div>
</c:if>

<c:if test="${requestScope.repos == null}">
    <h2 class="text-center">${requestScope.user.login} doesn't have any repositories</h2>
</c:if>

</body>
</html>
